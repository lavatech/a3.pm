## a3.pm

a3.pm is an ejabberd instance that intends to be up to date, with as many stock ejabberd features enabled as possible to give the best possible user experience.

This repo contains the config file with minimal differences (we simply remove passwords etc) and the website files.

Feel free to base your own ejabberd config on this ours. Search for `# lavatech:` in `ejabberd.yml` to see which parts you'll need to modify.

For maximum compliance, you'll need to include equivalent dns records to those in `dns-records` file and include equivalents of the files under `website/.well-known` on a server hosted on your hostname.

---

## ejabberd version

Please keep in mind that we often make use of features that are only available in the latest ejabberd version, so it might not be possible to run the config on anything before the latest release.

Right now, a3.pm ejabberd runs a version of ejabberd we built ourselves with `make distclean && ./autogen.sh && ./configure --disable-elixir --enable-pgsql && make && sudo make install && sudo chmod +x /usr/local/sbin/ejabberdctl && sudo chown ejabberd:ejabberd /usr/local/sbin/ejabberdctl && sudo systemctl restart ejabberd` (thanks a LOT to people on the ejabberd MUC for help with this), as the 18.12 version of ejabberd had various bugs that required us to compile from source, but this is no longer the case.

You're recommended to just use the version packaged for your distro, but if you feel like compiling ejabberd like we are, feel free to do so.

## PostgreSQL

We use PostgreSQL, so you'll need to import at least [the base scheme](https://github.com/processone/ejabberd/blob/master/sql/pg.new.sql) to be able to run this properly.

If you're moving data from an existing database (for ex. mnesia) to postgresql, you might be interested in [`ejd2sql:export(<<"hostname">>, sql)`](https://docs.ejabberd.im/admin/databases/mysql/#migrating-data-from-internal-database-to-mysql) (this is what I used as other one kept timing out) or [`ejabberdctl export2sql $hostname $filename`](https://docs.ejabberd.im/admin/guide/managing/#list-of-ejabberd-commands).

## S2S Blocks

We as a3.pm signed the ["The Jabber Spam Fighting Manifesto"](https://github.com/JabberSPAM/jabber-spam-fighting-manifesto) by jabberSPAM.

To provide the best experience to our users, we follow [jabberSPAM's server blacklist](https://github.com/JabberSPAM/blacklist) by disabling federation with those servers.

We recommend keeping said blocks enabled if you're basing your config on ours, but if you want to disable it, see `bad_servers` section in `ejabberd.yml`.

---

### Licenses

- The config is released under GNU General Public License v2.0 as the config file we're basing this on (ejabberd default) is using that license.
- The `dns-records` file is released to public domain, by @ao.

Under `website/`:

- The `conversejs.html` file is released under GPLv2 license, by @a.
- The `xmpp.html` and `lavaxmpp.css` files are released under GPLv2, by @slice.
- The files under `.well-known` are released to public domain, by @a and @luna.
